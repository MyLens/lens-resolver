// import SessionAdapter, { PutFileOptions, GetFileOptions, UserData } from '../source/session-adapter';
// import crypto from 'crypto'
// export const MOCK_DOMAIN = "https://somewhere.dne/";


// export function hashSign(dataString: string, dummyPrivate: string): string {
//     return crypto.createHash('sha256').update(dataString).digest('hex');
// }

// export function hashVerify(dataString: string, signature: string, dummyPublic: string): boolean {
//     return crypto.createHash('sha256').update(dataString).digest('hex') === signature;
// }

// export function pubKeyFromPrivKeyDummy(priveKey: string): string {
//     return "Some Public Key";
// }

// // For testing with local memory
// export default class MockUserSession implements SessionAdapter {
//     private files: Array<{ fileName: string, data: string }> = []

//     userData : UserData
//     constructor(userData?: UserData){
//         if (userData) this.userData = userData
//         else {
//             this.userData = {
//                 appPrivateKey: "Some Private key",
//                 profile: { name: "Home Simpson" },
//                 identityAddress: "blahblah",
//                 username: "mockuser.id.blockstack"
//             }
//         }
//     }

//     private numPutFileCalls: number = 0;
//     getNumPutFileCalls(): number {
//         return this.numPutFileCalls;
//     }
//     private numGetFileCalls: number = 0;
//     getNumGetFileCalls(): number {
//         return this.numGetFileCalls;
//     }
//     private numDeleteFileCalls: number = 0;
//     getNumDeleteFileCalls(): number {
//         return this.numDeleteFileCalls;
//     }
//     private numGetFileUrlCalls: number = 0;
//     getNumGetFileUrlCalls(): number {
//         return this.numGetFileUrlCalls;
//     }
//     resetNumCalls(): void {
//         this.numPutFileCalls = 0;
//         this.numGetFileCalls = 0;
//         this.numGetFileUrlCalls = 0;
//         this.numDeleteFileCalls = 0;
//     }

//     putFile(path: string, content: string, options?: PutFileOptions): Promise<string> {
//         this.numPutFileCalls = this.numPutFileCalls + 1;
//         return new Promise((resolve, reject) => {
//             if (content == "" && options != null && options!.encrypt === false) throw new Error("500 error! bad day! (gaia doesn't allow unencrypted empty files)");
//             let file = this.files.find(f => f.fileName == path);
//             if (!file) {
//                 file = { fileName: path, data: "" };
//                 this.files.push(file);
//             }
//             setTimeout(() => {
//                 file!.data = content;
//                 resolve();
//             }, 150);
//         });
//     }

//     getFile(path: string, options?: GetFileOptions): Promise<string | null> {
//         this.numGetFileCalls = this.numGetFileCalls + 1;

//         return new Promise((resolve, reject) => {
//             const file = this.files.find(f => f.fileName == path);
//             if (!file) resolve(null);
//             else {
//                 setTimeout(() => {
//                     resolve(file.data);
//                 }, 100);
//             }
//         });
//     }

//     deleteFile(path: string): Promise<void> {
//         this.numDeleteFileCalls = this.numDeleteFileCalls + 1;

//         return new Promise((resolve, reject) => {
//             const file = this.files.find(f => f.fileName == path);
//             if (!file) reject(new Error("File does not exist!"));
//             else {
//                 this.files = this.files.filter(x => x.fileName !== path ? x : undefined)
//                 setTimeout(() => {
//                     resolve();
//                 }, 100);
//             }
//         });
//     }

//     getFileUrl(path: string): Promise<string> {
//         this.numGetFileUrlCalls = this.numGetFileUrlCalls + 1;
//         return new Promise((resolve, reject) => {
//             const file = this.files.find( f => {
//                 return f.fileName == path
//             });
//             if (!file) reject(new Error("File does not exist!"));
//             else {
//                 resolve(MOCK_DOMAIN + file.fileName);
//             }
//         });
//     }

//     loadUserData(): UserData {
//         return this.userData
//     }

//     encryptContent(content: string | Buffer, options?: {publicKey?: string}) : string {
//         if (content instanceof Buffer) return content.toString()
//         else return content
//     }
    
//     decryptContent(content: string, options?: object): string | Buffer {
//         return content
//     }
    
//     listFiles(options?: { pathFilter?: string, callback?: (name: string) => boolean }): Promise<Array<string>> { 
//         throw new Error("Not implemented"); 
//     }

// }