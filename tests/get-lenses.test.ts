// import LensStorage, {
//     MockUserSession, 
//     hashSign, 
//     pubKeyFromPrivKeyDummy, 
//     MOCK_DOMAIN,
//     Data
// } from '@mylens/lens-storage'; 

// import FetchAdapter from '../source/fetch-adapter'; 
// import { createGetLenses } from '../source/get-lens';

// export class MockFetchAdapter extends MockUserSession implements FetchAdapter {
//     async fetch(url: string) : Promise<any> {
//         const results: (null | string) = await this.getFile(url.replace(MOCK_DOMAIN, ""));
//         if(results == null) return null;
//         return JSON.parse(results);
//     }
// }

// test("Test batch lens resolution.", async ()=> {
//     const session = new MockFetchAdapter(); 
//     const storage = new LensStorage(session, hashSign, pubKeyFromPrivKeyDummy); 
//     // Create two pieces of data. 
//     const d1 = await storage.createData({ name: 'phoneNumber', type: 'string/phone-number', data: '+15051234567' }); 
//     const d2 = await storage.createData({ name: 'firstName', type: 'string', data: 'Homer' }); 
//     const d3 = await storage.createData({ name: 'lastName', type: 'string', data: 'Simpson' }); 

//     const l1 = await storage.createLens({label: "Lens 1"}, 'Another Id', [d1.id, d2.id]);
//     const l2 = await storage.createLens({label: "Lens 2"}, 'Another Id', [d2.id, d3.id]);

//     // Create a Lens resolver
//     const getLenses = createGetLenses(session); 
//     const decryptContent = (content: string) : string => {
//         return session.decryptContent(content); 
//     }

//     session.resetNumCalls();

//     const resolved = await getLenses([
//         {url: l1.url, decrypt: decryptContent}, 
//         {url: l2.url, decrypt: decryptContent},
//         {url: "https://somebullshit.asdasdasdx7ya892d7.com", decrypt: decryptContent}
//     ]); 

//     expect(resolved.length).toBe(3);

//     expect(resolved[0]).not.toBeNull();
//     expect(resolved[0]!.data.length).toBe(2);
//     expect(resolved[0]!.data[0].data).toBe("+15051234567");
//     expect(resolved[0]!.data[1].data).toBe("Homer");

//     expect(resolved[1]).not.toBeNull();
//     expect(resolved[1]!.data.length).toBe(2);
//     expect(resolved[1]!.data[0].data).toBe("Homer");
//     expect(resolved[1]!.data[1].data).toBe("Simpson");

//     expect(resolved[2]).toBeNull();

//     // There should be one attempt on each lens file, and then only one
//     // on the shared data file.
//     expect(session.getNumGetFileCalls()).toBe(4);
// });

test("Nothing", () => { expect(true).toBe(true); })