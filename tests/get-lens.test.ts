// import LensStorage, {
//     MockUserSession, 
//     hashSign, 
//     pubKeyFromPrivKeyDummy, 
//     MOCK_DOMAIN,
//     Data
// } from '@mylens/lens-storage'; 

// import FetchAdapter from '../source/fetch-adapter'; 
// import { createGetLens } from '../source/get-lens';

// export class MockFetchAdapter extends MockUserSession implements FetchAdapter {
//     async fetch(url: string) : Promise<any> {
//         const results: (null | string) = await this.getFile(url.replace(MOCK_DOMAIN, ""));
//         if(results == null) return null;
//         return JSON.parse(results);
//     }
// }

// test("Test composite lens creation and resolution.", async ()=> {
//     let session = new MockFetchAdapter(); 
//     let storage = new LensStorage(session, hashSign, pubKeyFromPrivKeyDummy); 
//     // Create two pieces of data. 
//     let d1 = await storage.createData({ name: 'phoneNumber', type: 'string/phone-number', data: '+15051234567' }); 
//     let d2 = await storage.createData({ name: 'firstName', type: 'string', data: 'Homer' }); 

//     // Create a composite Lens.
//     let compositeData = await storage.createData({ name: 'emergencyContact', type: 'composite', children: [d1.id, d2.id]}); 
//     let compositeLens = await storage.createLens({label: "Lens to emergency contact"}, 'Another Id', [compositeData.id]);
    
//     // Create a Lens resolver
//     const getLens = createGetLens(session); 
//     const decryptContent = (content: string) : string => {
//         return session.decryptContent(content); 
//     }

//     const resolved = await getLens(compositeLens.url, decryptContent); 

//     expect(resolved).not.toBeNull();
//     expect(resolved!.data.length).toBe(1);
//     // console.log('resolved: ', JSON.stringify(resolved, null, 2)); 

//     /*
//      * For composite types, the Data value must be intepreted as an array of data values.  
//      */
//     expect((resolved!.data[0].children!).find(x  => x.name === 'phoneNumber')).not.toBe(undefined);
//     expect((resolved!.data[0].children!).find(x  => x.name === 'firstName')).not.toBe(undefined); 
 
// });

// test("Test multi-level composite resolution. ", async () => {
//     let session = new MockFetchAdapter(); 
//     let storage = new LensStorage(session, hashSign, pubKeyFromPrivKeyDummy); 
//     // Create one pieces of data. 
//     let d1 = await storage.createData({ name: 'phoneNumber', type: 'string/phone-number', data: '+15051234567' }); 
//     // Level 1 composite value
//     let c1 = await storage.createData({ name: 'compositeL1', type: 'composite', children: [d1.id] }); 

//     let c2 = await storage.createData({ name: 'compsiteL2', type: 'composite', children: [c1.id] }); 

//     let l3 = await storage.createLens({label: "Lens to compositeL2"}, 'another id2', [c2.id]); 

//     const getLens = createGetLens(session); 
//     const decryptContent = (content: string) : string => {
//         return session.decryptContent(content); 
//     }

//     const resolved = await getLens(l3.url, decryptContent);
//     // console.log('resolved = ', JSON.stringify(resolved, null, 2)); 
//     expect(resolved).not.toBeNull();
//     expect(resolved!.data.length).toBe(1); 
//     expect((resolved!.data[0].children!).length).not.toBe(undefined); 

//     let resolvedLayer1 = resolved!.data[0].children!;
//     expect(resolvedLayer1[0].name).toBe("compositeL1");  
//     // console.log('resolved layer1 ', resolvedLayer1[0]); 
//     expect((resolvedLayer1[0].children!).length).toBe(1); 

//     let finalValue = resolvedLayer1[0].children!;

//     // console.log('finalValue = ', finalValue);
//     expect(finalValue[0].name).toBe('phoneNumber'); 
// })

// test("Test delete lens resolution.", async () => {
//     let session = new MockFetchAdapter(); 
//     let storage = new LensStorage(session, hashSign, pubKeyFromPrivKeyDummy); 

//     let l3 = await storage.createLens({label: "Lens To Delete"}, 'dead', []); 

//     // Delete the lens.
//     await storage.deleteLens(l3.id);

//     const getLens = createGetLens(session); 
//     const decryptContent = (content: string) : string => {
//         return session.decryptContent(content); 
//     }

//     const resolved = await getLens(l3.url, decryptContent);
//     expect(resolved).toBeNull();

// })

// test("Test delete data in lens resolution.", async () => {
//     let session = new MockFetchAdapter(); 
//     let storage = new LensStorage(session, hashSign, pubKeyFromPrivKeyDummy); 

//     let d1 = await storage.createData({ name: 'firstName', type: 'string', data: 'Homor' }); 
//     let d2 = await storage.createData({ name: 'lastName', type: 'string', data: 'Simpson' }); 

//     let l3 = await storage.createLens({label: "Lens To Delete"}, 'dead', [d1.id, d2.id]); 

//     // Delete a data.
//     await storage.deleteData(d2.id);

//     const getLens = createGetLens(session); 
//     const decryptContent = (content: string) : string => {
//         return session.decryptContent(content); 
//     }

//     const resolved = await getLens(l3.url, decryptContent);
//     expect(resolved).not.toBeNull();
//     expect(resolved!.data.length).toBe(1);
//     expect(resolved!.data[0].name).toBe('firstName');

// })

// // import { createGetLens } from '../source/get-lens';
// // import FetchAdapter from '../source/fetch-adapter';
// // import MockUserSession, { MOCK_DOMAIN, hashSign, pubKeyFromPrivKeyDummy} from '../mock-user-session';
// // import { Data } from '@mylens/lens-types';
// // import { ResolvedLens } from '../source/resolved-lens'



// // test('A lens is downloaded', async () => {
// //     const adapter = new MockFetchAdapter();
// //     const getLens = createGetLens(adapter);
    
// //     const givenNameProp = "givenName";    
// //     const givenName = "Alice";

// //     const familyNameProp = "familyName";
// //     const familyName = "Smith";

// //     const telephoneProp = "telephone";
// //     const telephone = "+12223456789";
    
// //     const emailProp = "email";
// //     const email = "alice_smith@lens.io";

// //     const addressProp = "address";
// //     const address = "101 Broadway Ave NE Albuquerque, NM 87102, United States";

// //     const ids: Array<string> = [];
// //     let data: null | Data = null;

// //     data = await lensStorage.createData(givenNameProp, "Text", givenName);
// //     ids.push(data.id);
// //     data = await lensStorage.createData(familyNameProp, "Text", familyName);
// //     ids.push(data.id);
// //     data = await lensStorage.createData(telephoneProp, "Text", telephone);
// //     ids.push(data.id);
// //     data = await lensStorage.createData(emailProp, "Text", email);
// //     ids.push(data.id);
// //     data = await lensStorage.createData(addressProp, "Text", address);
// //     ids.push(data.id);

// //     const users = await lensStorage.createAnonymousLens(["Bob"], { dataIds: ids }, undefined, [{ [ids[0]]: "firstName" }]);
// //     expect(users.length).toBe(1);

// //     const url = users[0].filename;
// //     const accessToken = users[0].publicKey;
// //     let lens : RemoteLens
// //     if (accessToken){
// //         lens = await getAnonymousLens(url, accessToken);
// //     } else {
// //         throw new Error('There is no key provided!')
// //     }
// //     lens = await getAnonymousLens(url, accessToken);

// //     expect(lens.id).toBe(users[0].id);
// //     expect(lens.userId).toBe(lensStorage.userId);
// //     expect(lens.publicKey).toBe(lensStorage.publicKey);

 
// //     expect(lens.data.find(d => d.name == givenNameProp)!.data).toBe(givenName);
// //     expect(lens.data.find(d => d.name == familyNameProp)!.data).toBe(familyName);
// //     expect(lens.data.find(d => d.name == telephoneProp)!.data).toBe(telephone);
// //     expect(lens.data.find(d => d.name == emailProp)!.data).toBe(email);
// //     expect(lens.data.find(d => d.name == addressProp)!.data).toBe(address);
// // });

test("Nothing", () => { expect(true).toBe(true); })