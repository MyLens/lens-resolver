import { createGetLens, createGetLenses } from './source/get-lens';
import { AxiosFetchAdapter } from './source/fetch-adapter';

export { createGetLenses };
export { createGetLens };  
export { default as ResolvedLens } from './source/resolved-lens'; 
export { default as Data } from './source/data';
export { AliasMap } from '@mylens/lens-types';

export const getLens = createGetLens(new AxiosFetchAdapter());
export const getLenses = createGetLenses(new AxiosFetchAdapter());
