import axios from 'axios';

export default interface FetchAdapter  {
    fetch(url: string): Promise<any>
}

export class AxiosFetchAdapter implements FetchAdapter {
    async fetch(url: string) : Promise<any> {
        const result = await axios.get(url);
        return result.data;
    }
}