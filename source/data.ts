import { Vendor } from '@mylens/lens-types';

/**
 * The decrypted contents of a piece of lens data.
 * 
 * Should look exactly like the Data from @mylens/lens-types,
 * except that the children is an array of Data, instead of a list of
 * LensDataIndexes.
 */
export default interface Data {
    /**
     * The ID of the data. This is a UUID.
     */
    id: string,
    /**
     * The name given to this piece of data. Note this is not unique, but
     * intended to be toolable for matching similar data types.
     * E.g. 'firstName', 'lastName', 'emailAddress'
     */
    name: string, 
    /**
     * The type of content this data holds.
     * E.g. 'Text', 'Email', 'PhoneNumber'
     */
    type: string, 
    /**
     * The content, value or actual data held by the data. This depends
     * on the type, and of course what was put in here. 
     */
    data: any, 
    /**
     * If this data type has references to one or more child data,
     * they are available here
     */
    children?: Array<Data>,
    /**
     * The public key of the data owner. Useful for deriving the signature.
     */
    publicKey: string, 
    /**
     * The signature of the data, to verify integrity.
     */
    signature: string, 
    /**
     * A list of vendor verifications for this data.
     */
    verifiedBy: Vendor[], 
}