import { FileSource, CachedFileSource } from '@mylens/lens-file-source';
import { Data, Lens, LensDataIndex, AliasMap, LensContent } from '@mylens/lens-types';
import ResolvedData from './data';
import { symmetricDecrypt, EncryptedMetaData } from '@mylens/lens-crypto';

import FetchAdapter from './fetch-adapter'
import HttpFileSource from './http-file-source';
import ResolvedLens from './resolved-lens';


async function fetchUrls(urls: string[], adapter: FetchAdapter) : Promise<Array<Lens>> {

    let responsePromises = urls.map(url => adapter.fetch(url));
    // Avoid using Promise.all here; we want failures to not break everyone.
    // If one URL is bad, push 'null'.
    let responses: Array<any> = [];
    for (let i = 0; i < responsePromises.length; i++) {
        try {
            const r = responsePromises[i];
            const result = await r;
            responses.push(result);
        } catch (error) {
            responses.push(null);
        }
    }

    return responses;
}

async function parseLensData(
    fileSource: FileSource,
    content: string
): Promise<{ aliases?: AliasMap, data: Data[] }> {
    const jContent = JSON.parse(content);
    if(jContent instanceof Array) {
        return await parseLegacyLensData(fileSource, jContent as LensDataIndex[]);
    } else {
        const lensContent = jContent as LensContent;
        const data = await parseLensDataIndicies(fileSource, lensContent.data);
        return {
            data: data,
            aliases: lensContent.aliases
        }
    }
}    

async function parseLegacyLensData(
    fileSource: FileSource,
    indicies: LensDataIndex[]
): Promise<{ aliases: AliasMap, data: Data[] }> {

    const data = await parseLensDataIndicies(fileSource, indicies);

    // Populate name map from legacy format. 
    const aliases = indicies.reduce( (prev, i) => {
   
        if (i.alias !== undefined) {

            if (typeof i.alias === 'string'){
                prev[i.id] = [{ alias: i.alias }];
            }
        }   
        return prev;
    }, {} as AliasMap);

    return { aliases: aliases, data: data };
}

async function parseLensDataIndicies(
    fileSource: FileSource,
    indicies: Array<LensDataIndex>
): Promise<Data[]> {

    const dataResult: Array<Data | null> = await Promise.all(indicies.map(async i => {
        const contents = await fileSource.getFile(i.fileName, false);
        if (contents) {
            try {
                const jFileContents = JSON.parse(contents);
                const sData = jFileContents[i.id];
                let jContents: EncryptedMetaData | null;
                // We need to be careful here...
                // If an 'old' user hasn't upgraded their data, their data may be stored as id: any (old format) instead of id: string (new format)
                // Check for both conditions.
                if (typeof sData === 'string') jContents = JSON.parse(jFileContents[i.id]);
                else jContents = jFileContents[i.id];

                if (jContents) {
                    let data: Data = JSON.parse(symmetricDecrypt(i.key, jContents));

                    return data;
                }
            } catch (e) {
                // If we fail to parse or fail to decrypt, then they don't
                // get that piece of data.
                console.error("Failed to parse decrypted contents.", e);
            }
        }
        return null;
    }));
    const data = dataResult.filter(d => d != null) as Array<Data>;
    return data;
}


async function getLensesImpl(
    toGet: Array<{ url: string, decrypt: (encryptedContent: string) => string }>, 
    adapter: FetchAdapter, 
) : Promise<Array<ResolvedLens|null>> {

    const responses = await fetchUrls(toGet.map(g => g.url), adapter);
    // Note: Need to use HttpFileSource to access data on someone else's storage
    // Note: We want to create a new one each time so we don't get old data through
    // the lifetime of the app.
    const fileSource: FileSource = new CachedFileSource(new HttpFileSource(adapter));
    const lensPromises = responses.map(async (r,i) => {
        // If we fail to get a Lens URL, then it probably doesn't exist.
        if(!r) return null;

        const decrypt = toGet[i].decrypt;

        let dataResult: ResolvedLens | null = null;

        try {
            let content: string = "";

            content = decrypt(r!.content!);

            let fdata = await parseLensData(fileSource, content);

            // Go through and update any children with the resolved data in children, recursively
            const walkFn : (data: Array<Data>) => Promise<Array<ResolvedData>> = async (data) => {
                return await Promise.all(data.map(async d => {
                    const ret : ResolvedData = {
                        id: d.id,
                        name: d.name, 
                        type: d.type, 
                        data: d.data, 
                        publicKey: d.publicKey, 
                        signature: d.signature, 
                        verifiedBy: d.verifiedBy 
                    };
                    if(d.children) {
                        const rChildren = await parseLensDataIndicies(fileSource, d.children); 
                        ret.children = await walkFn(rChildren); 
                    }

                    return ret;
                }));
            };
            const resolvedData = await walkFn(fdata.data);

            dataResult = {
                id: r.id!,
                userId: r.userId!,
                publicKey: r.publicKey!,
                data: resolvedData,
                created: r.created!,
                updated: r.updated!,
                aliases: fdata.aliases || {}
            }
        } catch (error) {
            throw new Error("Failed while resolving lens: " + error);
        }

        return dataResult!;
    });

    return await Promise.all(lensPromises);
}

export function createGetLens(adapter: FetchAdapter)
: (
    url: string, 
    decrypt: (encryptedContent: string) => string, 
) => Promise<ResolvedLens|null> {

    return async function getLens(
        url: string, 
        decrypt: (encryptedContent: string) => string, 
    ) : Promise<ResolvedLens|null> {
        let lenses = await getLensesImpl([{url, decrypt}], adapter);
        return await lenses[0];
    }
}

export function createGetLenses(adapter: FetchAdapter)
: (
    requests: Array<{ url: string, decrypt: (encryptedContent: string) => string }>, 
) => Promise<Array<ResolvedLens|null>> {

    return async function getLenses(
        requests: Array<{ url: string, decrypt: (encryptedContent: string) => string }>, 
    ) : Promise<Array<ResolvedLens|null>> {
        return await getLensesImpl(requests, adapter);
    }
}