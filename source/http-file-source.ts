import FetchAdapter from './fetch-adapter';
import { FileSource } from '@mylens/lens-file-source';

export default class HttpFileSource implements FileSource {
    private fetchAdapter: FetchAdapter;
    constructor(fetchAdapter: FetchAdapter) {
        this.fetchAdapter = fetchAdapter;
    }
    async getFile(path: string, decrypt: boolean): Promise<string | null> { 
        const contents = await this.fetchAdapter.fetch(path);
        if(!contents) {
            return null;
        } else {
            return JSON.stringify(contents);
        } 
    }

    async putFile(path: string, content: string, encrypt: boolean): Promise<void> { 
        throw new Error("Not implemented");
    }
    async deleteFile(path: string) : Promise<void> { 
        throw new Error("Not implemented");
    }
    async listFiles(options?: { pathFilter?: string, callback?: (name: string) => boolean }): Promise<Array<string>> { 
        throw new Error("Not implemented"); 
    }
    async getFileUrl(path: string): Promise<string | null> { 
        throw new Error("Not implemented");
    }
}