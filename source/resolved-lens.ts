import { AliasMap } from '@mylens/lens-types';
import Data from './data';

// Should be a matchup to the @mylens/lens-types Lens type
// Differences are, all fields here are required, we've added
// 'data', and omitted 'content' ('content' is encrypted 'data')
export default interface ResolvedLens {
    id: string,
    userId: string,
    publicKey: string,
    data: Array<Data>,
    created: number,
    updated: number,
    aliases: AliasMap,
}