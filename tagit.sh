#!/usr/bin/env bash

PROJECTID="13943753"
VERSION=`node -e "console.log(require('./package').version)"`
POST="tag_name=$VERSION&ref=master"
curl --header "Private-Token: $GITLAB_TOKEN" --data $POST https://gitlab.com/api/v4/projects/$PROJECTID/repository/tags
